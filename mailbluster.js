var getFeedback = function getFeedback(isSuccess, result, successText) {
  return "<div class=\"alert alert-".concat(isSuccess ? 'success' : 'danger', " alert-dismissible fade show\" role=\"alert\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n      ").concat(isSuccess ? successText : result.message || result.email, "\n    </div>");
};

$(document).ready(function () {
  var $forms = $('.mailbluster-subscribe');

  if ($forms.length > 0) {
    $forms.each(function (index, value) {
      var $form = $(value);
      var $submit = $form.find('[type=\'submit\']');
      var $feedback = $form.find('.mailbluster-feedback');
      var successText = $form.find('[type=\'hidden\']').val() || 'Thank you so much for subscribing!';
      var submitText = $submit.text();
      $form.on('submit', function (e) {
        e.preventDefault();
        $submit.text('Please wait...');
        $.ajax({
          type: 'POST',
          url: '/assets/php/mailbluster.php',
          data: $form.serialize() // again, keep generic so this applies to any form,

        }).done(function (result) {
          $feedback.html(getFeedback(true, result, successText));
          $form.trigger('reset');
        }).fail(function (xhr) {
          $feedback.html(getFeedback(false, xhr.responseJSON));
        }).always(function () {
          $submit.text(submitText);
        });
      });
    });
  }
});